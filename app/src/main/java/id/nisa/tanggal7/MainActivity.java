package id.nisa.tanggal7;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    //Inisialisasi Pertama di Java
    FrameLayout profil;
    FrameLayout kota;
    FrameLayout edu;
    FrameLayout family;
    FrameLayout main;
    FrameLayout keluar;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Inisialisasi Kedua di Java
        profil = (FrameLayout) findViewById(R.id.profil);
        kota = (FrameLayout) findViewById(R.id.kota);
        edu = (FrameLayout)findViewById(R.id.edu);
        family = (FrameLayout) findViewById(R.id.family);
        main = (FrameLayout) findViewById(R.id.main);
        keluar = (FrameLayout) findViewById(R.id.keluar);
        long lastPress;
        Toast backpressToast;

        //Button Profil Ketika DiKlik
        profil.setOnClickListener(new View.OnClickListener()
                                  {
                                      @Override
                                      public void onClick(View v)
                                      {
                                          Toast.makeText(getApplicationContext(), "Profil Telah Dipilih", Toast.LENGTH_SHORT).show();
                                          Intent beach = new Intent(MainActivity.this, Profile.class);
                                          startActivity(beach);
                                      }
                                  }
        );

        //Button MyCity Ketika DiKlik
        kota.setOnClickListener(new View.OnClickListener()
                                {
                                    @Override
                                    public void onClick(View v)
                                    {
                                        Toast.makeText(getApplicationContext(), "MyCity Telah Dipilih", Toast.LENGTH_SHORT).show();
                                        Intent beach = new Intent(MainActivity.this, MyCity.class);
                                        startActivity(beach);
                                    }
                                }
        );

        //Button MyEducation Ketika DiKlik
        edu.setOnClickListener(new View.OnClickListener()
                               {
                                   @Override
                                   public void onClick(View v)
                                   {
                                       Toast.makeText(getApplicationContext(), "MyEducation Telah Dipilih", Toast.LENGTH_SHORT).show();
                                       Intent beach = new Intent(MainActivity.this, Education.class);
                                       startActivity(beach);
                                   }
                               }
        );

        //Button MyFamily Ketika DiKlik
        family.setOnClickListener(new View.OnClickListener()
                                  {
                                      @Override
                                      public void onClick(View v)
                                      {
                                          Toast.makeText(getApplicationContext(), "MyFamily Telah Dipilih", Toast.LENGTH_SHORT).show();
                                          Intent beach = new Intent(MainActivity.this, Family.class);
                                          startActivity(beach);
                                      }
                                  }
        );

        //Button Quiz Ketika DiKlik
        main.setOnClickListener(new View.OnClickListener()
                                {
                                    @Override
                                    public void onClick(View v)
                                    {
                                        Toast.makeText(getApplicationContext(), "Quiz Telah Dipilih", Toast.LENGTH_SHORT).show();
                                        Intent beach = new Intent(MainActivity.this, Quiz.class);
                                        startActivity(beach);
                                    }
                                }
        );

        //Button Exit Ketika DiKlik
        keluar.setOnClickListener(new View.OnClickListener()
                                  {
                                      @Override
                                      public void onClick(View v)
                                      {
                                          Toast.makeText(getApplicationContext(), "Aplikasi Keluar", Toast.LENGTH_SHORT).show();
                                          finish();
                                          System.exit(0);
                                      }
                                  }
        );


    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Closing Activity")
                .setMessage("Are you sure you want to close this activity?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }

                })
                .setNegativeButton("No", null)
                .show();

    }

    public void Quiz(View view) {
    }
}
