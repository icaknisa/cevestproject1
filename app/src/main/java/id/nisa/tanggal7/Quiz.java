package id.nisa.tanggal7;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;


public class Quiz extends AppCompatActivity {

    EditText ed1;
    TextView tv1, tv2, tv3;
    RadioButton a, b, c, d;
    Button bt;
    RadioGroup rg;
    ImageView emot_senyum, emot_sedih, emot_marah;
    int q, s;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);
        ed1 = (EditText) findViewById(R.id.name);
        tv1 = (TextView) findViewById(R.id.ques);
        tv2 = (TextView) findViewById(R.id.response);
        tv3 = (TextView) findViewById(R.id.score);
        rg = (RadioGroup) findViewById(R.id.optionGroup);
        a = (RadioButton) findViewById(R.id.option1);
        b = (RadioButton) findViewById(R.id.option2);
        c = (RadioButton) findViewById(R.id.option3);
        d = (RadioButton) findViewById(R.id.option4);
        bt = (Button) findViewById(R.id.next);
        emot_senyum = (ImageView) findViewById(R.id.emot_senyum);
        emot_sedih = (ImageView) findViewById(R.id.emot_sedih);
        emot_marah = (ImageView) findViewById(R.id.emot_marah);
        q = 0;
        s = 0;

    }

    public void Quiz(View v) {
        switch (q) {
            case 0: {
                ed1.setVisibility(View.INVISIBLE);
                rg.setVisibility(View.VISIBLE);
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);

                tv2.setText("");
                tv3.setText("");

                a.setEnabled(true);
                b.setEnabled(true);
                c.setEnabled(true);
                d.setEnabled(true);
                ed1.setEnabled(true);
                bt.setText("Next");
                s = 0;

                tv1.setText("1. (5x5)-2=");
                a.setText("23");
                b.setText("21");
                c.setText("18");
                d.setText("8");
                q = 1;
                break;
            }
            case 1: {
                ed1.setVisibility(View.INVISIBLE);
                ed1.setEnabled(false);
                tv1.setText("2.Jarak bumi ke Matahari");
                a.setText("100 juta km");
                b.setText("149,6 juta km");
                c.setText("200 juta km");
                d.setText("50 juta km");

                if (a.isChecked()) {
                    JawabanBenar();
                } else if (!a.isChecked() && !b.isChecked() && !c.isChecked() && !d.isChecked()) {
                    JawabanTidakDipilih();

                } else if (!a.isChecked()) {
                    JawabanSalah();
                }
                q = 2;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                break;
            }

            case 2: {
                ed1.setVisibility(View.INVISIBLE);
                tv1.setText("(49:7)-8=?");
                a.setText("-5");
                b.setText("1");
                c.setText("-6");
                d.setText("-1");


                if (b.isChecked()) {
                    JawabanBenar();
                } else if (!a.isChecked() && !b.isChecked() && !c.isChecked() && !d.isChecked()) {
                    JawabanTidakDipilih();
                } else if (!b.isChecked()) {
                    JawabanSalah();
                }
                q = 3;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                break;
            }

            case 3: {
                ed1.setVisibility(View.INVISIBLE);
                tv1.setText("4. Apa rumus luas segitiga?");
                a.setText("a x t");
                b.setText("a + t");
                c.setText("(a x t)/2");
                d.setText("t/2");

                if (d.isChecked()) {
                    JawabanBenar();

                } else if (!a.isChecked() && !b.isChecked() && !c.isChecked() && !d.isChecked()) {
                    JawabanTidakDipilih();
                } else if (!d.isChecked()) {
                    JawabanBenar();
                }
                q = 4;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                break;
            }

            case 4: {
                ed1.setVisibility(View.INVISIBLE);
                tv1.setText("5.Indonesia Merdeka?");
                a.setText("1944");
                b.setText("1969");
                c.setText("1954");
                d.setText("1945");


                if (c.isChecked()) {
                    JawabanBenar();

                } else if (!a.isChecked() && !b.isChecked() && !c.isChecked() && !d.isChecked()) {
                    JawabanTidakDipilih();

                } else if (!c.isChecked()) {
                    JawabanSalah();
                }
                q = 5;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                break;
            }

            case 5: {
                ed1.setVisibility(View.INVISIBLE);
                tv1.setText("6. Hari Kesaktian Pancasila?");
                a.setText("1 Oktober");
                b.setText("5 Desember");
                c.setText("17 Agustus");
                d.setText("1 Juni");


                if (a.isChecked()) {
                    JawabanBenar();

                } else if (!a.isChecked() && !b.isChecked() && !c.isChecked() && !d.isChecked()) {
                    JawabanTidakDipilih();

                } else if (!a.isChecked()) {
                    JawabanSalah();
                }
                q = 6;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                break;
            }
            case 6: {
                ed1.setVisibility(View.INVISIBLE);
                tv1.setText("7. Yang bukan merupakan hewan ?");
                a.setText("Angsa");
                b.setText("Bebek");
                c.setText("Anyelir");
                d.setText("Koala");


                if (c.isChecked()) {
                    JawabanBenar();

                } else if (!a.isChecked() && !b.isChecked() && !c.isChecked() && !d.isChecked()) {
                    JawabanTidakDipilih();


                } else if (!c.isChecked()) {
                    JawabanSalah();

                }
                q = 7;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                break;
            }
            case 7: {
                ed1.setVisibility(View.INVISIBLE);
                tv1.setText("8.Ibukota Malaysia ?");
                a.setText("Kuching");
                b.setText("Kuala Lumpur");
                c.setText("Penang");
                d.setText("Ipoh");


                if (c.isChecked()) {
                    JawabanBenar();


                } else if (!a.isChecked() && !b.isChecked() && !c.isChecked() && !d.isChecked()) {
                    JawabanTidakDipilih();


                } else if (!c.isChecked()) {
                    JawabanSalah();

                }
                q = 8;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                break;
            }
            case 8: {
                ed1.setVisibility(View.INVISIBLE);
                tv1.setText("9.Letak Kawasan Wisata Mandeh");
                a.setText("Pesisir Selatan");
                b.setText("Aceh");
                c.setText("Jambi");
                d.setText("Rimbo Bujang");


                if (b.isChecked()) {
                    JawabanBenar();


                } else if (!a.isChecked() && !b.isChecked() && !c.isChecked() && !d.isChecked()) {
                    JawabanTidakDipilih();


                } else if (!b.isChecked()) {
                    JawabanSalah();

                }
                q = 9;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                break;
            }
            case 9: {
                ed1.setVisibility(View.INVISIBLE);
                tv1.setText("10.(200:2)-15= ?");
                a.setText("81");
                b.setText("83");
                c.setText("85");
                d.setText("125");


                if (a.isChecked()) {
                    JawabanBenar();


                } else if (!a.isChecked() && !b.isChecked() && !c.isChecked() && !d.isChecked()) {
                    JawabanTidakDipilih();


                } else if (!a.isChecked()) {
                    JawabanSalah();

                }
                q = 10;
                a.setChecked(false);
                b.setChecked(false);
                c.setChecked(false);
                d.setChecked(false);
                break;
            }
            case 10: {
                ed1.setVisibility(View.INVISIBLE);
                a.setEnabled(false);
                b.setEnabled(false);
                c.setEnabled(false);
                d.setEnabled(false);
                if (c.isChecked())
                {
                    JawabanBenar();

                } else if (!a.isChecked() && !b.isChecked() && !c.isChecked() && !d.isChecked()) {
                    JawabanTidakDipilih();


                } else if (!c.isChecked()) {
                    JawabanSalah();
                }
                tv3.setText(ed1.getText() + "'s final score is " + s);
                bt.setText("Restart");
                q = 0;
                break;
            }
        }
    }
    //Buat 1 Class Biar Tinggal Dipanggil Saja Jika Jawaban Benar
    public void JawabanBenar()
    {
        tv2.setText("Jawaban Benar");
        s=s+50;
        emot_senyum.setVisibility(View.VISIBLE);
        emot_senyum.postDelayed(new Runnable() {
            @Override
            public void run() {
                emot_senyum.setVisibility(View.INVISIBLE);
            }
        }, 3000);
    }

    //Buat 1 Class Biar Tinggal Dipanggil Saja Jika Jawaban Benar
    public void JawabanTidakDipilih()
    {
        tv2.setText("Jawaban Tidak Dipilih");
        s=s+0;
        emot_marah.setVisibility(View.VISIBLE);
        emot_marah.postDelayed(new Runnable() {
            @Override
            public void run() {
                emot_marah.setVisibility(View.INVISIBLE);
            }
        }, 3000);
    }

    //Buat 1 Class Biar Tinggal Dipanggil Saja Jika Jawaban Salah
    public void JawabanSalah()
    {
        tv2.setText("Jawaban Salah");
        s=s-10;
        emot_sedih.setVisibility(View.VISIBLE);
        emot_sedih.postDelayed(new Runnable() {
            @Override
            public void run() {
                emot_sedih.setVisibility(View.INVISIBLE);
            }
        }, 3000);
    }

}




